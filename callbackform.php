<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$APPLICATION->IncludeComponent(
    "picom:main.feedback", 
    ".default", 
    array(
        "EMAIL_TO" => "lebedeva@dev.picom.ru",
        "EVENT_MESSAGE_ID" => array(
            0 => "8",
        ),
        "OK_TEXT" => "Спасибо. Администратор свяжется с Вами в ближайшее время.",
        "REQUIRED_FIELDS" => array(
            0 => "NAME",
            1 => "TEL",
            2 => "EMAIL",
            3 => "MESSAGE",
        ),
        "USE_CAPTCHA" => "N",
        "COMPONENT_TEMPLATE" => ".default",
        "AJAX_MODE" => "Y",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_ADDITIONAL" => "undefined",
		"AJAX_OPTION_SHADOW" => "N", // затемнять область
    ),
    false
);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?>