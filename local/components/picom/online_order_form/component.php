<?php
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();

/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponent $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */

$arResult["PARAMS_HASH"] = md5(serialize($arParams).$this->GetTemplateName());

$arParams["USE_CAPTCHA"] = (($arParams["USE_CAPTCHA"] != "N" && !$USER->IsAuthorized()) ? "Y" : "N");

$arParams["EVENT_NAME"] = trim($arParams["EVENT_NAME"]);
if($arParams["EVENT_NAME"] == '') {
	$arParams["EVENT_NAME"] = "ONLINE_ORDER_FORM";	
}
   
$arParams["EMAIL_TO"] = trim($arParams["EMAIL_TO"]);
if($arParams["EMAIL_TO"] == '') {
	$arParams["EMAIL_TO"] = COption::GetOptionString("main", "email_from");
}
    
$arParams["OK_TEXT"] = trim($arParams["OK_TEXT"]);
if($arParams["OK_TEXT"] == '') {
	$arParams["OK_TEXT"] = "Спасибо, ваше сообщение принято.";
}
    
if ($_SERVER["REQUEST_METHOD"] == "POST" && 
    $_POST["submit"] <> '' && (
        !isset($_POST["PARAMS_HASH"]) || 
        $arResult["PARAMS_HASH"] === $_POST["PARAMS_HASH"]
    )
) {
	$arResult["ERROR_MESSAGE"] = array();
	
	if(check_bitrix_sessid()) {
		if(empty($arParams["REQUIRED_FIELDS"]) || !in_array("NONE", $arParams["REQUIRED_FIELDS"])) {
				
			if ((empty($arParams["REQUIRED_FIELDS"]) || in_array("TEL", $arParams["REQUIRED_FIELDS"])) && 
                strlen($_POST["user_tel"]) <= 1
            ) {
				$arResult["ERROR_MESSAGE"][] = "Укажите ваш телефон.";
				$arResult["ERROR_FIELDS"]["TEL"] = "error";
			}
			
			if((empty($arParams["REQUIRED_FIELDS"]) || in_array("MESSAGE", $arParams["REQUIRED_FIELDS"])) && strlen($_POST["MESSAGE"]) <= 1)
			{
				$arResult["ERROR_MESSAGE"][] = "Вы не написали сообщение.";
				$arResult["ERROR_FIELDS"]["MESSAGE"] = "error";
			}
		}
		// if(strlen($_POST["user_tel"]) > 1 && !check_tel($_POST["user_tel"]))
        if ((empty($arParams["REQUIRED_FIELDS"]) || in_array("TEL", $arParams["REQUIRED_FIELDS"])) && 
            strlen($_POST["user_tel"]) < 1)
		{
			$arResult["ERROR_MESSAGE"][] = "Указанный телефон некорректен.";
			$arResult["ERROR_FIELDS"]["TEL"] = "error";
		}
		if($arParams["USE_CAPTCHA"] == "Y")
		{
			include_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/captcha.php");
			$captcha_code = $_POST["captcha_sid"];
			$captcha_word = $_POST["captcha_word"];
			$cpt = new CCaptcha();
			$captchaPass = COption::GetOptionString("main", "captcha_password", "");
			if (strlen($captcha_word) > 0 && strlen($captcha_code) > 0) {
				if (!$cpt->CheckCodeCrypt($captcha_word, $captcha_code, $captchaPass)) {
					$arResult["ERROR_MESSAGE"][] = "Неверно указан код защиты от автоматических сообщений.";
                }
			}
            else {
				$arResult["ERROR_MESSAGE"][] = "Не указан код защиты от автоматических сообщений.";
            }

		}
		if(empty($arResult["ERROR_MESSAGE"]))
		{
			//отправка письма
			$arFields = Array(
				"AUTHOR_TEL" => $_POST["user_tel"],
				"TEXT" => $_POST["MESSAGE"],
                "PRODUCT"  => $_POST['PRODUCT_ID']
			);
			if(!empty($arParams["EVENT_MESSAGE_ID"]))
			{
				foreach($arParams["EVENT_MESSAGE_ID"] as $v)
					if(IntVal($v) > 0)
						CEvent::Send($arParams["EVENT_NAME"], SITE_ID, $arFields, "N", IntVal($v));
			}
			else
				CEvent::Send($arParams["EVENT_NAME"], SITE_ID, $arFields);
				
			//добавление в инфоблок
			CModule::IncludeModule("iblock");
			$el = new CIBlockElement;
			$elementId = $el->Add(array(
				"MODIFIED_BY"       => $USER->GetID(),
				"IBLOCK_SECTION_ID" => false,
				"IBLOCK_ID"         => 13,
				"PROPERTY_VALUES"   => array(
					"tel"      => $_POST["user_tel"],
					"message"  => $_POST["MESSAGE"],
                    "product"  => $_POST['PRODUCT_ID']
				),
				"NAME"              => "Новый онлайн-заказ",
				"ACTIVE"            => "Y"
			));
            
			$_SESSION["MF_TEL"] = htmlspecialcharsbx($_POST["user_tel"]);
			$_SESSION["MF_MES"] = htmlspecialcharsbx($_POST["MESSAGE"]);
			
			LocalRedirect($APPLICATION->GetCurPageParam("success=".$arResult["PARAMS_HASH"], Array("success")));
		}
		$arResult["MESSAGE"] = htmlspecialcharsbx($_POST["MESSAGE"]);
		$arResult["AUTHOR_TEL"] = htmlspecialcharsbx($_POST["user_tel"]);
	}
	else
		$arResult["ERROR_MESSAGE"][] = "Ваша сессия истекла. Отправьте сообщение повторно.";
}
elseif($_REQUEST["success"] == $arResult["PARAMS_HASH"])
{
	$arResult["OK_MESSAGE"] = $arParams["OK_TEXT"];
}

if(empty($arResult["ERROR_MESSAGE"]))
{
	if(strlen($_SESSION["MF_TEL"]) > 0)
		$arResult["AUTHOR_TEL"] = htmlspecialcharsbx($_SESSION["MF_TEL"]);
	if(strlen($_SESSION["MF_MES"]) > 0)
		$arResult["MESSAGE"] = htmlspecialcharsbx($_SESSION["MF_MES"]);
}

if($arParams["USE_CAPTCHA"] == "Y")
	$arResult["capCode"] =  htmlspecialcharsbx($APPLICATION->CaptchaGetCode());

$arResult['PRODUCT'] = $arParams['PRODUCT'];
// echo '<pre>';
// print_r($_POST);
// echo '</pre>';


$this->IncludeComponentTemplate();


// function check_tel($p)
// {
	// //+7 (999) 999-9999
	// $reg = '/\+7\s\(\d{3}\)\s\d{3}\-\d{4}/';
	// preg_match($reg, $p, $arr);
	// if ($arr[0] == $p) return true;
	// else return false;
// }
