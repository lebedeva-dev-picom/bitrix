<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$site = ($_REQUEST["site"] <> ''? $_REQUEST["site"] : ($_REQUEST["src_site"] <> ''? $_REQUEST["src_site"] : false));

$arFilter = Array("TYPE_ID" => "CALLBACKFORM", "ACTIVE" => "Y");
if($site !== false)
	$arFilter["LID"] = $site;

$arEvent = Array();

$dbType = CEventMessage::GetList($by="ID", $order="DESC", $arFilter);
while($arType = $dbType->GetNext())
	$arEvent[$arType["ID"]] = "[".$arType["ID"]."] ".$arType["SUBJECT"];

//arEvent - список почтовых шаблонов


$arComponentParameters = array(
	"PARAMETERS" => array(
		// "AJAX_MODE" => array(),
		"USE_CAPTCHA" => Array(
			"NAME" => "Использовать защиту от автоматических сообщений (CAPTCHA) для неавторизованных пользователей", 
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "Y", 
			"PARENT" => "BASE",
		),
		"OK_TEXT" => Array(
			"NAME" => "Сообщение, выводимое пользователю после отправки", 
			"TYPE" => "STRING",
			"DEFAULT" => "Спасибо, ваше сообщение принято.", 
			"PARENT" => "BASE",
		),
		"EMAIL_TO" => Array(
			"NAME" => "E-mail, на который будет отправлено письмо", 
			"TYPE" => "STRING",
			"DEFAULT" => htmlspecialcharsbx(COption::GetOptionString("main", "email_from")), 
			"PARENT" => "BASE",
		),
		"REQUIRED_FIELDS" => Array(
			"NAME" => "Обязательные поля для заполнения", 
			"TYPE"=>"LIST", 
			"MULTIPLE"=>"Y", 
			"VALUES" => Array(
				"NONE" => "(все необязательные)", 
				"NAME" => "Имя", 
				"TEL" => "Телефон",
				"EMAIL" => "E-mail", 
				"MESSAGE" => "Сообщение"
			),
			"DEFAULT"=>"", 
			"COLS"=>25, 
			"PARENT" => "BASE",
		),

		"EVENT_MESSAGE_ID" => Array(
			"NAME" => "Почтовые шаблоны для отправки письма", 
			"TYPE"=>"LIST", 
			"VALUES" => $arEvent,
			"DEFAULT"=>"", 
			"MULTIPLE"=>"Y", 
			"COLS"=>25, 
			"PARENT" => "BASE",
		),

	)
);


?>