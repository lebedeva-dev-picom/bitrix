<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => "Форма Заказ звонка",
	"DESCRIPTION" => "Форма для отправки сообщения с сайта на E-mail",
	"ICON" => "/images/feedback.gif",
	"PATH" => array(
		"ID" => "utility",
	),
);
?>
