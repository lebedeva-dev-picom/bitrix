<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
$this->addExternalCss(SITE_TEMPLATE_PATH . '/styles.css');
$this->addExternalJS(SITE_TEMPLATE_PATH . '/scripts/dev.js');
?>

<div class="mfeedback">

<h3>Заказ звонка</h3>

<?if(!empty($arResult["ERROR_MESSAGE"]))
{
	foreach($arResult["ERROR_MESSAGE"] as $v)
		ShowError($v);
}
if(strlen($arResult["OK_MESSAGE"]) > 0)
{
	?><div class="mf-ok-text"><?=$arResult["OK_MESSAGE"]?></div><?
}
?>

<form action="<?=POST_FORM_ACTION_URI?>" method="POST">
<?=bitrix_sessid_post()?>
	<div class="mf-name">
		<div class="mf-text">
			Имя<?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("NAME", $arParams["REQUIRED_FIELDS"])):?><span class="mf-req">*</span><?endif?>
		</div>
		<input class="<?=$arResult["ERROR_FIELDS"]["NAME"]?>" type="text" name="user_name" value="<?=$arResult["AUTHOR_NAME"]?>">
	</div>

	<div class="mf-tel">
		<div class="mf-text">
			Телефон<?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("TEL", $arParams["REQUIRED_FIELDS"])):?><span class="mf-req">*</span><?endif?>
		</div>
		<input id="phone" placeholder="+7 (___) ___-____" type="text" name="user_tel" <?=(isset($arResult["AUTHOR_TEL"]) ? ' value="'.$arResult["AUTHOR_TEL"].'"' : '')?><?=(isset($arResult["ERROR_FIELDS"]["TEL"]) ? ' class="'.$arResult["ERROR_FIELDS"]["TEL"].'"' : '')?> />
	</div>
	
	<div class="mf-email">
		<div class="mf-text">
			E-mail<?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("EMAIL", $arParams["REQUIRED_FIELDS"])):?><span class="mf-req">*</span><?endif?>
		</div>
		<input class="<?=$arResult["ERROR_FIELDS"]["EMAIL"]?>" type="text" name="user_email" value="<?=$arResult["AUTHOR_EMAIL"]?>">
	</div>

	<div class="mf-message">
		<div class="mf-text">
			Сообщение<?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("MESSAGE", $arParams["REQUIRED_FIELDS"])):?><span class="mf-req">*</span><?endif?>
		</div>
		<textarea class="<?=$arResult["ERROR_FIELDS"]["MESSAGE"]?>" name="MESSAGE" rows="5" cols="40"><?=$arResult["MESSAGE"]?></textarea>
	</div>

	<?if($arParams["USE_CAPTCHA"] == "Y"):?>
	<div class="mf-captcha">
		<div class="mf-text">Защита от автоматических сообщений</div>
		<input type="hidden" name="captcha_sid" value="<?=$arResult["capCode"]?>">
		<img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["capCode"]?>" width="180" height="40" alt="CAPTCHA">
		<div class="mf-text">Введите слово на картинке<span class="mf-req">*</span></div>
		<input type="text" name="captcha_word" size="30" maxlength="50" value="">
	</div>
	<?endif;?>
	
	<input type="hidden" name="PARAMS_HASH" value="<?=$arResult["PARAMS_HASH"]?>">
	<input type="submit" name="submit" value="Отправить">
</form>
</div>