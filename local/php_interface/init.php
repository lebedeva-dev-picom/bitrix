<?php
AddEventHandler("iblock", "OnAfterIBlockElementAdd", Array("AfterElementAdd", "AfterElementAddSendMail"));

class AfterElementAdd
{
    public function AfterElementAddSendMail(&$arFields)
    {
        if($arFields["IBLOCK_ID"] == 9) //Вопрос-Ответ
        {
            $arEventFields = array(
                "NAME"    => $arFields["NAME"],
                "MESSAGE" => $arFields["PREVIEW_TEXT"],
            );
            CEvent::Send("VOPROS_FORM", SITE_ID, $arEventFields);
        }
    }
}

class CExamDebug
{
    public static function show_me_value($a)
    {
        echo '<pre>';
        var_dump($a);
        echo '</pre>';
    }
}

function checkTel($p)
{
    //+7 (999) 999-9999
    $reg = '/\+7\s\(\d{3}\)\s\d{3}\-\d{4}/';
    return preg_match($reg, $p) == 1;
}

?>