<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?IncludeTemplateLangFile(__FILE__);?><!DOCTYPE HTML>
<html lang="en-US">
<head>
	<meta charset="utf8">
	
	
	<?$APPLICATION->ShowHead();?>
	<title><?$APPLICATION->ShowTitle()?></title>
	
	<?
	$APPLICATION->AddHeadString('<link rel="stylesheet" href="'.SITE_TEMPLATE_PATH.'/template_style.css" />', true);
	
	$APPLICATION->AddHeadString('<!--[if gte IE 9]><style type="text/css">.gradient {filter: none;}</style><![endif]-->', true);
	
	$APPLICATION->AddHeadString('<link rel="stylesheet" media="screen" href="'.SITE_TEMPLATE_PATH.'/scripts/fancyBox/source/jquery.fancybox.css" />', true);
	?>

</head>
<body>
	<div id="panel"><?$APPLICATION->ShowPanel();?></div>
	<div class="wrap">
		<div class="hd_header_area">
			<div class="hd_header">
				<table>
					<tr>
						<td rowspan="2" class="hd_companyname">
							<?$cur_dir=$APPLICATION->GetCurDir();?>
							<h1><a href="<?=SITE_DIR?>" <?if($cur_dir=='/s2/company/'||$cur_dir=='/s2/products/'):?>class="count_companyname"<?endif?>>
							<?$APPLICATION->IncludeComponent(
								"bitrix:main.include", 
								".default", 
								array(
									"AREA_FILE_RECURSIVE" => "Y",
									"AREA_FILE_SHOW" => "sect",
									"AREA_FILE_SUFFIX" => "include/section-title",
									"EDIT_TEMPLATE" => "",
									"COMPONENT_TEMPLATE" => ".default"
								),
								false
							);?>
							</a></h1>
						</td>
						<td rowspan="2" class="hd_txarea">
							<span class="tel">
								<?$APPLICATION->IncludeComponent(
									"bitrix:main.include", 
									".default", 
									array(
										"AREA_FILE_RECURSIVE" => "Y",
										"AREA_FILE_SHOW" => "sect",
										"AREA_FILE_SUFFIX" => "include/tel",
										"EDIT_TEMPLATE" => "",
										"COMPONENT_TEMPLATE" => ".default"
									),
									false
								);?>
							</span>
							<br><?=GetMessage('T_WORKING_HOURS')?>
							<span class="workhours">
							<?$APPLICATION->IncludeComponent(
								"bitrix:main.include", 
								".default", 
								array(
									"AREA_FILE_RECURSIVE" => "Y",
									"AREA_FILE_SHOW" => "sect",
									"AREA_FILE_SUFFIX" => "include/workhours",
									"EDIT_TEMPLATE" => "",
									"COMPONENT_TEMPLATE" => ".default"
								),
								false
							);?></span>
						</td>
						<td style="width:232px">
							 <?$APPLICATION->IncludeComponent("bitrix:search.form", "search-form", Array(
								"PAGE" => "#SITE_DIR#search/index.php",
								"USE_SUGGEST" => "N"
							));?>
						</td>
					</tr>
					<tr>
						<td style="padding-top: 11px;">
							<?$APPLICATION->IncludeComponent(
							"bitrix:system.auth.form", "auth-form", 
							Array(
								"FORGOT_PASSWORD_URL" => "",
								"PROFILE_URL" => "",
								"REGISTER_URL" => "",
								"SHOW_ERRORS" => "N"
							));?>
						</td>
					</tr>
				</table>
				<div class="nv_topnav">
					<?$APPLICATION->IncludeComponent(
						"bitrix:menu", 
						"horizontal_multilevel", 
						array(
							"ROOT_MENU_TYPE" => "top",
							"MAX_LEVEL" => "2",
							"CHILD_MENU_TYPE" => "left",
							"USE_EXT" => "Y",
							"MENU_CACHE_TYPE" => "A",
							"MENU_CACHE_TIME" => "36000000",
							"MENU_CACHE_USE_GROUPS" => "Y",
							"MENU_CACHE_GET_VARS" => array(
							),
							"COMPONENT_TEMPLATE" => "horizontal_multilevel",
							"DELAY" => "N",
							"ALLOW_MULTI_SELECT" => "N"
						),
						false,
						array(
							"ACTIVE_COMPONENT" => "Y"
						)
					);?>
				</div>
			</div>
		</div>
		
		<!--- // end header area --->
		
		<?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "breadcrumb", Array(
			"COMPONENT_TEMPLATE" => ".default",
				"PATH" => "",
				"SITE_ID" => "s1",
				"START_FROM" => "0",
			),
			false
		);?>
		
		<div class="main_container page">
			<div class="mn_container">
				<div class="mn_content">
					<div class="main_post">
						<div class="main_title">
							<h1><?$APPLICATION->ShowTitle()?></h1>
						</div>
