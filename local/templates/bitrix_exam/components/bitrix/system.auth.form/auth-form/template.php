<?if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();?>

<?if ($arResult['SHOW_ERRORS'] == 'Y' && $arResult['ERROR']&& $arResult['ERROR_MESSAGE']['TYPE'] == 'ERROR')
	ShowMessage($arResult['ERROR_MESSAGE']);?>

<?if($arResult["FORM_TYPE"] == "login"):?>

<span class="hd_singin">
	<a id="hd_singin_but_open" href="">Войти на сайт</a>
	<div class="hd_loginform">
		<span class="hd_title_loginform">Войти на сайт</span>
		<form name="system_auth_form<?=$arResult["RND"]?>" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">

			<?if($arResult["BACKURL"] <> ''):?>
			<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
			<?endif?>

			<?foreach ($arResult["POST"] as $key => $value):?>
			<input type="hidden" name="<?=$key?>" value="<?=$value?>" />
			<?endforeach?>

			<input type="hidden" name="AUTH_FORM" value="Y" />
			<input type="hidden" name="TYPE" value="AUTH" />
				
			<input type="text" name="USER_LOGIN" placeholder="<?=GetMessage("AUTH_LOGIN")?>" value="<?=$arResult["USER_LOGIN"]?>" />

			<input type="password" name="USER_PASSWORD" autocomplete="off" placeholder="<?=GetMessage("AUTH_PASSWORD")?>" >
					
			<?if($arResult["SECURE_AUTH"]):?>
			<span class="bx-auth-secure" id="bx_auth_secure<?=$arResult["RND"]?>" title="<?echo GetMessage("AUTH_SECURE_NOTE")?>" style="display:none">
				<div class="bx-auth-secure-icon"></div>
			</span>
			<noscript>
			<span class="bx-auth-secure" title="<?echo GetMessage("AUTH_NONSECURE_NOTE")?>">
				<div class="bx-auth-secure-icon bx-auth-secure-unlock"></div>
			</span>
			</noscript>
			<script type="text/javascript">
			document.getElementById('bx_auth_secure<?=$arResult["RND"]?>').style.display = 'inline-block';
			</script>
			<?endif?>

			<noindex>
			<a href="<?=$arResult["AUTH_FORGOT_PASSWORD_URL"]?>" rel="nofollow" class="hd_forgotpassword"><?=GetMessage("AUTH_FORGOT_PASSWORD_2")?></a>
			</noindex>
				
				
			<?if ($arResult["STORE_PASSWORD"] == "Y"):?>
			<div class="head_remember_me" style="margin-top: 10px">
			<input type="checkbox" id="USER_REMEMBER_frm" name="USER_REMEMBER" value="Y" />
			<label for="USER_REMEMBER_frm" title="<?=GetMessage("AUTH_REMEMBER_ME")?>"><?echo GetMessage("AUTH_REMEMBER_SHORT")?></label>
			</div>
			<?endif?>
				
				
			<?if ($arResult["CAPTCHA_CODE"]):?>
			<div>
				<input type="hidden" name="captcha_sid" value="<?echo $arResult["CAPTCHA_CODE"]?>" />
				<img src="/bitrix/tools/captcha.php?captcha_sid=<?echo $arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" /><br /><br />
				<input type="text" name="captcha_word" maxlength="50" value="" placeholder="<?echo GetMessage("AUTH_CAPTCHA_PROMT")?>" />
			</div>
			<?endif?>
				
			<input type="submit" name="Login" value="<?=GetMessage("AUTH_LOGIN_BUTTON")?>" style="margin-top: 20px;" />
		</form>
		<span class="hd_close_loginform"><?=GetMessage("auth_form_close")?></span>
	</div>
</span>
<br>

<?if($arResult["NEW_USER_REGISTRATION"] == "Y"):?>
	<noindex><a href="<?=$arResult["AUTH_REGISTER_URL"]?>" rel="nofollow" class="hd_signup"><?=GetMessage("AUTH_REGISTER")?></a></noindex>
<?endif?>

<?if($arResult["AUTH_SERVICES"]):?>
	<?$APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "", 
	array(
		"AUTH_SERVICES"=>$arResult["AUTH_SERVICES"],
		"AUTH_URL"=>$arResult["AUTH_URL"],
		"POST"=>$arResult["POST"],
		"POPUP"=>"Y",
		"SUFFIX"=>"form",
	), 
	$component, 
	array("HIDE_ICONS"=>"Y")
	);?>
<?endif?>


<?
elseif($arResult["FORM_TYPE"] == "otp"):
?>

<form name="system_auth_form<?=$arResult["RND"]?>" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">
	<?if($arResult["BACKURL"] <> ''):?>
		<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
	<?endif?>

	<input type="hidden" name="AUTH_FORM" value="Y" />
	<input type="hidden" name="TYPE" value="OTP" />

	<?echo GetMessage("auth_form_comp_otp")?><br />
	<input type="text" name="USER_OTP" maxlength="50" value="" size="17" autocomplete="off" />
		
	<?if ($arResult["CAPTCHA_CODE"]):?>
		<?echo GetMessage("AUTH_CAPTCHA_PROMT")?>:<br />
		<input type="hidden" name="captcha_sid" value="<?echo $arResult["CAPTCHA_CODE"]?>" />
		<img src="/bitrix/tools/captcha.php?captcha_sid=<?echo $arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" /><br /><br />
		<input type="text" name="captcha_word" maxlength="50" value="" />
	<?endif?>

	<?if ($arResult["REMEMBER_OTP"] == "Y"):?>
		<input type="checkbox" id="OTP_REMEMBER_frm" name="OTP_REMEMBER" value="Y" />
		<label for="OTP_REMEMBER_frm" title="<?echo GetMessage("auth_form_comp_otp_remember_title")?>"><?echo GetMessage("auth_form_comp_otp_remember")?></label>
	<?endif?>

	<input type="submit" name="Login" value="<?=GetMessage("AUTH_LOGIN_BUTTON")?>" />
	
	<noindex><a href="<?=$arResult["AUTH_LOGIN_URL"]?>" rel="nofollow"><?echo GetMessage("auth_form_comp_auth")?></a></noindex>
	<br />
	
</form>


<?
else:
?>

<form action="<?=$arResult["AUTH_URL"]?>">
	<span class="hd_sing">
		<?=$arResult["USER_NAME"]?>
		[<a href="<?=$arResult["PROFILE_URL"]?>"><?=$arResult["USER_LOGIN"]?></a>]
	</span>

	<?foreach ($arResult["GET"] as $key => $value):?>
		<input type="hidden" name="<?=$key?>" value="<?=$value?>" />
	<?endforeach?>
	
	<input type="hidden" name="logout" value="yes" />
	<input style="border: 0px; background: none; text-decoration: underline; cursor: pointer;" class="hd_signup" type="submit" name="logout_butt" value="<?=GetMessage("AUTH_LOGOUT_BUTTON")?>" />			
	</br>
</form>

<?endif?>