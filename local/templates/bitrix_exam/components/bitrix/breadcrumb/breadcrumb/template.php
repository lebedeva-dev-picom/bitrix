<?if(!defined('B_PROLOG_INCLUDED')||B_PROLOG_INCLUDED!==true)die();

$strReturn = '';

if(!empty($arResult))
{
	$strReturn .= '<div class="bc_breadcrumbs"><ul>';
	$itemSize = count($arResult);
	for($index = 0; $index < $itemSize; $index++)
	{
		$title = htmlspecialcharsex($arResult[$index]['TITLE']);
		$href = $arResult[$index]['LINK'];
		$strReturn .= '<li><a href="'.$href.'">'.$title.'</a></li>';
	}
	$strReturn .= '</ul><div class="clearboth"></div></div>';
}

return $strReturn;
?>