<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?foreach($arResult["ITEMS"] as $arItem):?>
<?#$arItem=$arResult["ITEMS"][0]?>

<div class="sb_reviewed">

	<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?>
		<img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" class="sb_rw_avatar" alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"/>
	<?endif?>

	<?if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]):?>
		<span class="sb_rw_name"><?echo $arItem["NAME"]?></span>
	<?endif;?>

	<span class="sb_rw_job"><?=$arItem[PROPERTIES][position][VALUE]?><?if($arItem[PROPERTIES][company][VALUE]!=''):?> “<?=$arItem[PROPERTIES][company][VALUE]?>”<?endif?></span>

	<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" style="text-decoration:none">
	<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
		<p>“<?echo $arItem["PREVIEW_TEXT"];?>”</p>
	<?endif;?>	
	</a>
	
	<div class="clearboth"></div>
	<div class="sb_rw_arrow"></div>
</div>

<?endforeach?>
