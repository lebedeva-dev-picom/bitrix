<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?IncludeTemplateLangFile(__FILE__);?>

					</div>
				</div>
				<div class="sb_sidebar">
					<?$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"menu_left", 
	array(
		"ALLOW_MULTI_SELECT" => "N",
		"CHILD_MENU_TYPE" => "left",
		"COMPONENT_TEMPLATE" => "menu_left",
		"DELAY" => "N",
		"MAX_LEVEL" => "1",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"ROOT_MENU_TYPE" => "left",
		"USE_EXT" => "Y"
	),
	false
);?>
					
					<!--<div class="sb_event">
						<div class="sb_event_header"><h4>Ближайшие события</h4></div>
						<p><a href="">29 августа 2012, Москва</a></p>
						<p>Семинар производителей мебели России и СНГ, Обсуждение тенденций.</p>
					</div>-->
					
					<!--div class="sb_event">
						<div class="sb_event_header"><h4>Информация</h4></div>
						<p>Семинар производителей мебели России и СНГ, Обсуждение тенденций.</p>
					</div-->
					

					<?$APPLICATION->IncludeFile(
						SITE_DIR."index_include/sb_event.php",
						Array(),
						Array("MODE"=>"php")
					);?>
					
					<div class="sb_action">
						<a href=""><img src="<?=SITE_TEMPLATE_PATH?>/content/11.png" alt=""/></a>
						<h4>Акция</h4>
						<h5><a href="">Мебельная полка всего за 560 Р</a></h5>
						<a href="" class="sb_action_more">Подробнее &rarr;</a>
					</div>
					
					<?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"review", 
	array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "/s2/company/reviews/#ELEMENT_CODE#/",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "27",
		"IBLOCK_TYPE" => "media",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "N",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "1",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "position",
			1 => "company",
			2 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"COMPONENT_TEMPLATE" => "review",
		"TEMPLATE_THEME" => "blue",
		"MEDIA_PROPERTY" => "",
		"SLIDER_PROPERTY" => "",
		"SEARCH_PAGE" => "/search/",
		"USE_RATING" => "N",
		"USE_SHARE" => "N"
	),
	false
);?>
					
				</div>
				<div class="clearboth"></div>
			</div>
		</div>

		<div class="ft_footer">
			<div class="ft_container">
			
				<div class="ft_about">
					<h4>О магазине</h4>
					<?$APPLICATION->IncludeComponent("bitrix:menu", "menu_bottom_exam", Array(
						"ALLOW_MULTI_SELECT" => "N",
						"CHILD_MENU_TYPE" => "bottom",
						"COMPONENT_TEMPLATE" => "menu_bottom_exam",
						"DELAY" => "N",
						"MAX_LEVEL" => "1",
						"MENU_CACHE_GET_VARS" => array(),
						"MENU_CACHE_TIME" => "3600",
						"MENU_CACHE_TYPE" => "A",
						"MENU_CACHE_USE_GROUPS" => "Y",
						"ROOT_MENU_TYPE" => "bottom_exam",
						"USE_EXT" => "N"
					));?>
				</div>
				
<!-- callbackform -->
<script type="text/javascript">
$(function() {
	$("#callbackform_link").fancybox();
});
</script>
<div style="display:none" class="fancybox-hidden"><div id="callbackform">
<?$APPLICATION->IncludeComponent(
	"picom:main.feedback", 
	".default", 
	array(
		"EMAIL_TO" => "lebedeva@dev.picom.ru",
		"EVENT_MESSAGE_ID" => array(
			0 => "8",
		),
		"OK_TEXT" => "Спасибо. Администратор свяжется с Вами в ближайшее время.",
		"REQUIRED_FIELDS" => array(
			0 => "NAME",
			1 => "TEL",
			2 => "EMAIL",
			3 => "MESSAGE",
		),
		"USE_CAPTCHA" => "N",
		"COMPONENT_TEMPLATE" => ".default",
		"AJAX_MODE" => "Y",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "undefined"
	),
	false
);?>
</div></div>
<!-- callbackform end -->
				
				 <div class="ft_catalog">
					<h4>Каталог товаров</h4>
					<ul>
						<li><a href="">Кухни</a></li>
						<li><a href="">Кровати и кушетки</a></li>
						<li><a href="">Гарнитуры</a></li>
						<li><a href="">Тумобчки и прихожие</a></li>
						<li><a href="">Спальни и матрасы</a></li>
						<li><a href="">Аксессуары</a></li>
						<li><a href="">Столы и стулья</a></li>
						<li><a href="">Каталоги мебели</a></li>
						<li><a href="">Раскладные диваны</a></li>
						<li><a href="">Кресла</a></li>
					</ul>
					
				</div> 
				<div class="ft_contacts">
					<h4>Контактная информация</h4>
					<!-- vCard        http://help.yandex.ru/webmaster/hcard.pdf      -->
					<p class="vcard">
						<span class="adr">
							<span class="street-address">
							<?$APPLICATION->IncludeComponent('bitrix:main.include', '', Array(
								'AREA_FILE_RECURSIVE' => 'Y',
								'AREA_FILE_SHOW' => 'sect',
								'AREA_FILE_SUFFIX' => 'include/address',
								'EDIT_TEMPLATE' => ''
							));?></span>
						</span>
						
						<span class="tel">
						<?$APPLICATION->IncludeComponent('bitrix:main.include', '', Array(
							'AREA_FILE_RECURSIVE' => 'Y',
							'AREA_FILE_SHOW' => 'sect',
							'AREA_FILE_SUFFIX' => 'include/tel',
							'EDIT_TEMPLATE' => ''
						));?>
						</span>
						<strong>Время работы:</strong> <br/> <span class="workhours"><?$APPLICATION->IncludeComponent('bitrix:main.include', '', Array(
								'AREA_FILE_RECURSIVE' => 'Y',
								'AREA_FILE_SHOW' => 'sect',
								'AREA_FILE_SUFFIX' => 'include/workhours',
								'EDIT_TEMPLATE' => ''
							));?></span><br/>
					</p>
					<ul class="ft_solcial">
						<li><a href="" class="fb"></a></li>
						<li><a href="" class="tw"></a></li>
						<li><a href="" class="ok"></a></li>
						<li><a href="" class="vk"></a></li>
					</ul>

					<div class="ft_copyright">
						<?$APPLICATION->IncludeFile(
							SITE_DIR."include/copyright.php",
							Array(),
							Array("MODE"=>"html")
						);?>
					</div>
					
				</div>
				
				<div class="clearboth"></div>
			</div>
		</div>
	</div>

<div id="show_me_value">
<?#CExamDebug::show_me_value($APPLICATION)?>
</div>
	
</body>
<?
$APPLICATION->AddHeadString('<script type="text/javascript" src="'.SITE_TEMPLATE_PATH.'/js/jquery-1.8.2.min.js"></script>', true);
$APPLICATION->AddHeadString('<script type="text/javascript" src="'.SITE_TEMPLATE_PATH.'/js/functions.js"></script>', true);

$APPLICATION->AddHeadString('<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3/jquery.min.js"></script>', true);
$APPLICATION->AddHeadString('<script type="text/javascript" src="'.SITE_TEMPLATE_PATH.'/scripts/fancyBox/lib/jquery-1.9.0.min.js"></script>', true);
$APPLICATION->AddHeadString('<script type="text/javascript" src="'.SITE_TEMPLATE_PATH.'/scripts/fancyBox/lib/jquery-1.10.1.min.js"></script>', true);
$APPLICATION->AddHeadString('<script type="text/javascript" src="'.SITE_TEMPLATE_PATH.'/scripts/fancyBox/lib/jquery.mousewheel-3.0.6.pack.js"></script>', true);
$APPLICATION->AddHeadString('<script type="text/javascript" src="'.SITE_TEMPLATE_PATH.'/scripts/fancyBox/source/jquery.fancybox.pack.js"></script>', true);
$APPLICATION->AddHeadString('<script type="text/javascript" src="'.SITE_TEMPLATE_PATH.'/scripts/fancyBox/source/jquery.fancybox.js"></script>', true);

$APPLICATION->AddHeadString('<script type="text/javascript" src="'.SITE_TEMPLATE_PATH.'/scripts/maskedInput/jquery.maskedinput.min.js"></script>', true);
?>
</html>