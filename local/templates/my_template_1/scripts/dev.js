$(function(){

    $('#vopros').fancybox({
        'overlayShow': false,
        'scrolling' : 'no',
        'titleShow': false,
        'type': 'ajax',
        'href': '/vopros-otvet/popupform.php'
    });

    $('#callbackform_link').fancybox({
        'overlayShow': false,
        'scrolling' : 'no',
        'titleShow': false,
        'type': 'ajax',
        'href': '/callbackform.php'
    });
    
    $('#orderform_link').fancybox();
    
    $('#phone').mask('+7 (999) 999-9999');

});