<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
?>
			</div>
		</div>
		<div id="space-for-footer"></div>
	</div>
	
	
	<div id="footer">
	
		<div id="copyright">
			<?
			$APPLICATION->IncludeFile(
				SITE_DIR."include/copyright.php",
				Array(),
				Array("MODE"=>"html")
			);
			?>
		</div>

		<div class="footer-links">	
            <!--a href="" id="callbackform_link">Заказать звонок</a-->
			<?
			$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"catalog_horizontal_old", 
	array(
		"ROOT_MENU_TYPE" => "bottom",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_TIME" => "36000000",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MAX_LEVEL" => "1",
		"CHILD_MENU_TYPE" => "left",
		"USE_EXT" => "N",
		"ALLOW_MULTI_SELECT" => "N",
		"COMPONENT_TEMPLATE" => "catalog_horizontal_old",
		"DELAY" => "N",
		"MENU_THEME" => "site"
	),
	false
);
			?>
		</div>
		<div id="footer-design"><?=GetMessage("FOOTER_DISIGN")?></div>
	</div>


<!-- подключение ajax и fancybox -->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/scripts/fancyBox/lib/jquery-1.9.0.min.js"></script> 
<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/scripts/fancyBox/lib/jquery-1.10.1.min.js"></script>
<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/scripts/fancyBox/lib/jquery.mousewheel-3.0.6.pack.js"></script> 
<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/scripts/fancyBox/source/jquery.fancybox.pack.js"></script> 
<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/scripts/fancyBox/source/jquery.fancybox.js"></script>
<!-- подключение maskedinput -->
<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/scripts/maskedInput/jquery.maskedinput.min.js"></script>
<!-- dev.js -->
<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/scripts/dev.js"></script>
<!-- end -->


</body>
</html>